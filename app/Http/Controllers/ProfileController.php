<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Auth;

class ProfileController extends Controller
{
    public function index(){
        $profil = Profile::where('user_id', Auth::user()->id)->first();
        return view('profil.index', compact('profil'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'umur' => 'required',
            'alamat' => 'required',
        ]);

        $profil_data = [
            'umur' => $request->umur,
            'alamat' => $request->alamat,
        ];

        Profile::whereId($id)->update($profil_data);

        return redirect('/profil');
    }
}
