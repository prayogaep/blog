<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;
use App\Peran;
Use App\Film;
use DB;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peran = Peran::all();
        return view('peran.index', compact('peran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $film = Film::all();
        $cast = Cast::all();
        return view('peran.create', compact('film', 'cast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $peran = Peran::all();
        $this->validate($request, [
            'nama' => 'required',
            'film_id' => 'required',
            'cast_id' => 'required',
        ]);

        Peran::create([
            'nama' => $request->nama,
            'film_id' => $request->film_id,
            'cast_id' => $request->cast_id,
        ]);
        return redirect('/peran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = Peran::find($id);
        return view('peran.show', compact('peran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peran = Peran::find($id);
        $cast = Cast::all();
        $film = Film::all();
        return view('peran.edit', compact('peran','film' , 'cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'film_id' => 'required',
            'cast_id' => 'required',
        ]);

        $query = DB::table('peran')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'film_id' => $request["film_id"],
                'cast_id' => $request["cast_id"],
            ]);
        return redirect('/peran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('peran')->where('id', $id)->delete();
        return redirect('/peran');
    }
}
