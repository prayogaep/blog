<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Film;
use File;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:jpeg,jpg,png|max:2200',
            'genre_id' => 'required'
        ]);

        $gambar = $request->poster;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        Film::create([
            'judul' => $request->judul,
            'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'poster' => $new_gambar,
            'genre_id' => $request->genre_id
        ]);
        $gambar->move('img/' , $new_gambar);
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        return view('film.edit', compact('film' , 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "judul" => 'required',
            "ringkasan" => 'required',
            "tahun" => 'required',
            "poster" => 'mimes:jpeg,jpg,png|max:2200',
            "genre_id" => 'required',
        ]);

        $film = Film::findorfail($id);

        if($request->has('poster'))
        {
            $path = 'img/';
            File::delete($path . $film->poster);
            $gambar = $request->poster;
            $new_gambar = time() . ' - '. $gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);
            $film_data = [
                "judul" => $request->judul,
                "ringkasan" => $request->ringkasan,
                "tahun" => $request->tahun,
                "poster" => $new_gambar,
                "genre_id" => $request->genre_id,
            ];
        } else {
            $film_data = [
                "judul" => $request->judul,
                "ringkasan" => $request->ringkasan,
                "tahun" => $request->tahun,
                "genre_id" => $request->genre_id,
            ];
        }
        $film->update($film_data);

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::findorfail($id);
        $film->delete();

        $path = 'img/';
        File::delete($path . $film->poster);

        return redirect('/film');
    }
}
