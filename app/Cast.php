<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'cast';
    protected $fillable = ['nama', 'bio', 'umur', 'genre_id', 'tahun'];

    public function peran()
    {
        return $this->hasMany('App\Peran');
    }

    public function film()
    {
        return $this->belongsTo('App\Film');
    }
}
