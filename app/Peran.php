<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Peran extends Model
{
    protected $table = 'peran';
    protected $fillable = ['nama', 'cast_id', 'film_id'];
    public $timestamps = false;

    public function film()
    {
        return $this->belongsTo('App\Film');
    }
    
    public function cast()
    {
        return $this->belongsTo('App\Cast');
    }
    
}
