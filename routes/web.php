<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');



Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::resource('cast', 'CastController');
    Route::resource('genre', 'GenreController');
    Route::resource('peran', 'PeranController');
    Route::get('/', 'HomeController@home');
    Route::get('/register', 'AuthController@form');
    Route::post('/welcome', 'AuthController@kirim');
    Route::get('/table', function(){
        return view('items.table');
    });

    Route::get('/data-table', function(){
        return view('items.data-table');
    });
});

Route::resource('film', 'FilmController');
Route::resource('profil', 'ProfileController')->only([
    'index',
    'update'
]);
