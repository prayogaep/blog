@extends('layout.master')
@section('judul')
   <h1 class="mt-2 ml-2">Halaman Edit Genre</h1> 
@endsection

@section('content')
<section class="content-header">
</section>

  <!-- Main content -->
            <form action="/genre/{{$genre->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="nama">Nama Genre</label>
                    <input type="text" class="form-control" name="nama" value="{{$genre->nama}}" id="nama" placeholder="Masukkan Nama Cast">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
@endsection