@extends('layout.master')

@section('content')
<div class="container">
    <h1><b>Selamat Datang {{ $namadepan }} {{ $namabelakang }}</b></h1>
    <h2><b>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</b></h2>
</div>
@endsection