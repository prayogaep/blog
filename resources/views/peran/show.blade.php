@extends('layout.master')

@section('judul')
    Halaman Detail Peran Id {{$peran->id}}
@endsection

@section('content')
<h2>Show Post {{$peran->id}}</h2>
<h4>Nama Peran: {{$peran->nama}}</h4>
<p>Judul Film :{{$peran->film->judul}}</p>
<p>Nama Cast {{$peran->cast->nama}}</p>

<a href="/peran" class="btn btn-primary">Kembali</a>
@endsection