@extends('layout.master')

@section('content')
<a href="/peran/create" class="btn btn-primary my-2">Tambah</a>
<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">No</th>
        <th scope="col">Cast</th>
        <th scope="col">Film</th>
        <th scope="col">Nama Cast</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($peran as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                    <td>{{$value->cast->nama}}</td>
                    <td>{{$value->film->judul}}</td>
                    <td>{{$value->nama}}</td>
                {{-- <td>
                    <a href="/peran/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/peran/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/peran/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td> --}}
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection