@extends('layout.master')

@section('judul')
    Halaman Update Profile
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/g4mhcbddpbwzgbbof0zgm4t1jtf4nrw1acffbt5f0hkdsqnn/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });
</script>
@endpush

@section('content')
<form action="/profil/{{$profil->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" value="{{$profil->user->email}}" id="email" disabled>
    </div>
    <div class="form-group">
        <label for="name">Nama</label>
        <input type="text" class="form-control" name="name" value="{{$profil->user->name}}" id="name" disabled>
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$profil->umur}}" id="umur" placeholder="Masukkan Nama Cast">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="10">{{$profil->alamat}}</textarea>
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection