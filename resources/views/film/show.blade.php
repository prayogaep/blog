@extends('layout.master')

@section('judul')
    Halaman Update Film
@endsection

@section('content')

<div class="row">
  <div class="col">
    <div class="row">
      <div class="col">
        <img src="{{asset('img/' . $film->poster)}}" width="50%" alt="...">
      </div>
      <div class="col">
        <h4>List Cast</h4>
        <ul>
          @foreach ($film->peran as $item)
              <li> {{$item->cast->nama}} - {{$item->nama}}</li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="card mx-1">
    <div class="card-body">
      <h5 class="card-title">{{$film->judul}} ({{$film->tahun}})</h5>
      <p class="card-text"> {{$film->ringkasan}} </p>
      <a href="/film" class="btn btn-primary">Kembali</a>
      <br>
      <small>{{$film->created_at}}</small>
    </div>
  </div>
@endsection