@extends('layout.master')

@section('judul')
    Halaman Edit Film
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/g4mhcbddpbwzgbbof0zgm4t1jtf4nrw1acffbt5f0hkdsqnn/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });
</script>
@endpush

@section('content')
<h2>Edit Data Film</h2>
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" name="judul" id="judul"  value="{{$film->judul}}" placeholder="Masukkan Judul">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="ringkasan">Ringkasan</label>
        <textarea name="ringkasan" class="form-control" id="ringkasan" cols="30" rows="10">{{$film->ringkasan}}</textarea>
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="genre_id">Genre</label>
        <select class="form-control" name="genre_id" id="genre_id">
            <option value="">-- Pilih Genre --</option>
            @foreach ($genre as $item)
                @if ($item->id === $film->genre_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>    
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>    
                @endif
            @endforeach
        </select>
        @error('genre_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="poster">Poster</label>
        <input type="file" class="form-control" name="poster" id="poster">
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="tahun">Tahun Rilis</label>
        <input type="text" class="form-control" value="{{$film->tahun}}" name="tahun" id="tahun" placeholder="Masukkan Tahun">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection