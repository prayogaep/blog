@extends('layout.master')
@section('judul')
    Halaman List Film
@endsection

@section('content')

@auth
<a href="/film/create" class="btn btn-success my-2">Tambah Film</a>
    
@endauth
<div class="row">
  @foreach ($film as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('img/' . $item->poster)}}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">{{$item->judul}} ({{$item->tahun}}) <span class="badge badge-primary">{{$item->genre->nama}}</span></h5>
              <p class="card-text"> {{Str::limit($item->ringkasan, 100)}} </p>
              <form action="/film/{{$item->id}}" method="POST">
                @method('delete')
                @csrf
                @auth
                  <a href="/film/{{$item->id}}" class="btn btn-info">Read More</a>
                  <a href="/film/{{$item->id}}/edit" class="btn btn-success">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger">
                @endauth
                @guest
                  <a href="/film/{{$item->id}}" class="btn btn-info">Read More</a>
                @endguest
              </form>
              <br>
              <small>{{$item->created_at}}</small>
            </div>
          </div>
    </div>
    @endforeach
</div>

@endsection