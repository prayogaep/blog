@extends('layout.master')

@section('judul')
    Halaman Detail Cast Id {{$cast->id}}
@endsection

@section('content')
<h2>Show Post {{$cast->id}}</h2>
<h4>Nama : {{$cast->nama}}</h4>
<p>Umur :{{$cast->umur}}</p>
<p>Bio : {{$cast->bio}}</p>
@endsection