@extends('layout.master')
@section('judul')
   <h1 class="mt-2 ml-2">Halaman Tambah Cast</h1> 
@endsection

@section('content')
<section class="content-header">
</section>

  <!-- Main content -->
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama Cast</label>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Cast">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Nama Cast">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea class="form-control" name="bio" id="bio" cols="30" rows="10" placeholder="Masukan Bio Cast"></textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
@endsection